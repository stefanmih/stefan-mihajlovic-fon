<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
body{
padding:10px;
margin:10px;
}
</style>
<title>Studentska sluzba</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<p style="font-size: 30px;">Studentska sluzba</p><br/><br/>
<div class="form-group mb-2">
<table class="table">
<thead>
<tr>
<th>Rad sa profesorima</th>
<th>Rad sa studentima</th>
<th>Rad sa predmetima</th>
<th>Rad sa ispitima</th>
<th>Rad sa prijavama</th>
</tr>
</thead>
<tr>
<td><form action="${pageContext.servletContext.contextPath}/professor" method="post"><button class="btn btn-primary">Pregled</button></form></td>
<td><form action="${pageContext.servletContext.contextPath}/student" method="post"><button class="btn btn-primary">Pregled</button></form></td>
<td><form action="${pageContext.servletContext.contextPath}/subject" method="post"><button class="btn btn-primary">Pregled</button></form></td>
<td><form action="${pageContext.servletContext.contextPath}/exam" method="post"><button class="btn btn-primary">Pregled</button></form></td>
<td><form action="${pageContext.servletContext.contextPath}/registration" method="post"><button class="btn btn-primary">Pregled</button></form></td>
</tr>
</table>
</div>
</body>
</html>