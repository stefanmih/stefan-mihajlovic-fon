<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>OOPS. Stranica nije pronadjena. <a href="${pageContext.servletContext.contextPath}/index.jsp">Nazad na glavni meni.</a></h1>
</body>
</html>