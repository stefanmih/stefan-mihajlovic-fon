<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	var i = 0;
	$(document)
			.ready(
					function() {
						if ($('.error').length) {
							$('#container').slideDown();
						}
						$("#add").click(function() {
							if (i === 0) {
								$('#container').slideDown(1000);
								$('#container2').slideUp(1000);
								i = 1;
							} else {
								$('#container').slideUp(1000);
								i = 0;
							}
						});

						$("#all")
								.click(
										function() {
											window.location
													.replace("${pageContext.servletContext.contextPath}/exam/all");
										});
						$("#find").click(function() {
							if (i === 0) {
								$('#container2').slideDown(1000);
								$('#container').slideUp(1000);
								i = 1;
							} else {
								$('#container2').slideUp(1000);
								i = 0;
							}
						});
					});
</script>
<style type="text/css">
#addForm {
	display: none;
}

body {
	padding: 20px;
	margin: 20px;
}

#back {
	float: right;
}

.error {
	color: red;
	display: block;
}

#container {
	display: <%=((request.getAttribute("oldId") == null) ? "none" : "inline-block")%>;
	box-sizing: content-box;
	width: 300px;
	-webkit-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	padding: 10px;
}

#container2 {
	display: none;
	box-sizing: content-box;
	width: 300px;
	height: 140px;
	-webkit-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	padding: 10px;
}
</style>
<meta charset="ISO-8859-1">
<title>Kreiranje ispita</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body><div id="back">
	<form action="${pageContext.servletContext.contextPath}/index.jsp" method="post">
		<input type="submit" class="btn btn-success" value="Nazad na glavni meni"/>
	</form>
	</div>
	<div id="menu" class="form-group mb-2">
		<button id="add" class="btn btn-primary">Dodaj novi ispit</button>
		<button id="all" class="btn btn-primary">Lista ispita</button>
		<button id="find" class="btn btn-primary">Pronadji ispit</button>
	</div>
	<br />
	<div class="form-group" id="container">
		<form:form action="${pageContext.servletContext.contextPath}/exam/add"
			method="post" modelAttribute="ExamDto">
			<form:input path="id" readonly="true" type="hidden"
				value='<%=request.getAttribute("oldId")%>' />
			<label>Predmet:</label>
			<form:select class="form-control" path="subject.id">
				<c:forEach items="${subjectAll}" var="item">
					<form:option value="${item.id}">${item.name}</form:option>
				</c:forEach>
			</form:select>
			<label>Datum ispita:</label>
			<form:input class="form-control" type="date" path="date" />
			<label>Profesor:</label>
			<form:select class="form-control" path="professor.id">
				<c:forEach items="${professorAll}" var="item">
					<form:option value="${item.id}">${item.firstName} ${item.lastName}</form:option>
				</c:forEach>
			</form:select>
			<br />
			<button class="btn btn-primary" name="save">Sacuvaj</button>
		</form:form>
	</div>
	<div class="form-group" id="container2">
		<form action="${pageContext.servletContext.contextPath}/exam/all"
			method="post">
			<label>Predmet:</label> <input class="form-control" type="text"
				name="id" /><br /> <input class="btn btn-primary" type="submit"
				value="Nadji" />
		</form>
	</div>
</body>
</html>