<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	var i = 0;
	$(document).ready(function() {
		if($('.error').length){
			$('#container').slideDown();
		}
		$("#add").click(function() {
			if (i === 0) {
				$('#container').slideDown(1000);
				$('#container2').slideUp(1000);
				i = 1;
			} else {
				$('#container').slideUp(1000);
				i = 0;
			}
		});

		$("#all").click(function() {
			window.location.replace("${pageContext.servletContext.contextPath}/professor/all");
		});
		$("#find").click(function() {
			if (i === 0) {
				$('#container2').slideDown(1000);
				$('#container').slideUp(1000);
				i = 1;
			} else {
				$('#container2').slideUp(1000);
				i = 0;
			}
		});
	});
</script>
<style type="text/css">
body {
	padding: 20px;
	margin: 20px;
}

#container {
	display: <%=((request.getAttribute("oldId") == null) ? "none" : "inline-block")%>;
	box-sizing: content-box;
	width: 300px;
	-webkit-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	padding: 10px;
}
.error{
color:red;
display:block;
}

#container2 {
	display: none;
	box-sizing: content-box;
	width: 300px;
	height: 140px;
	-webkit-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	box-shadow: 14px 18px 30px -5px rgba(0, 0, 0, 0.75);
	padding: 10px;
}
</style>
<meta charset="ISO-8859-1">
<title>Dodavanje profesora</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<div style="float:right;">
	<form action="${pageContext.servletContext.contextPath}/index.jsp" method="post">
		<input type="submit" class="btn btn-success" value="Nazad na glavni meni"/>
	</form>
	</div>
	<div id="menu" class="form-group mb-2">
		<button id="add" class="btn btn-primary">Dodaj novog
			profesora</button>
		<button id="all" class="btn btn-primary">Lista profesora</button>
		<button id="find" class="btn btn-primary">Pronadji profesora</button>
	</div>
	<br />
	<div class="form-group" id="container">

		<form:form action="${pageContext.servletContext.contextPath}/professor/add" method="post"
			modelAttribute="ProfessorDto">
			<form:input path="id" readonly="true" type="hidden"
				value='<%=request.getAttribute("oldId")%>' />
			<label>Ime:</label>
			<form:input class="form-control" type="text" path="firstName" />
			<form:errors path="firstName" cssClass="error" />
			<label>Prezime:</label>
			<form:input class="form-control" type="text" path="lastName" />
			<form:errors path="lastName" cssClass="error" />
			<label>Email:</label>
			<form:input class="form-control" type="text" path="email" />
			<label>Adresa:</label>
			<form:input class="form-control" type="text" path="address" />
			<label>Grad:</label>
			<form:select class="form-control" path="city.code">
				<c:forEach items="${cityAll}" var="item">
					<form:option value="${item.code}">${item.code}, ${item.name}</form:option>
				</c:forEach>
			</form:select>
			<label>Telefon:</label>
			<form:input class="form-control" type="text" path="phone" />
			<label>Datum biranja:</label>
			<form:input class="form-control" type="date" path="reelectionDate" />
			<form:errors path="reelectionDate" cssClass="error" />
			<label>Zvanje</label>
			<form:select class="form-control" path="title.id">
				<c:forEach items="${titleAll}" var="item">
					<form:option value="${item.id}">${item.title}</form:option>
				</c:forEach>
			</form:select>
			<br />
			<button class="btn btn-primary" name="save">Sacuvaj</button>
		</form:form>
	</div>
	<div class="form-group" id="container2">
		<form action="${pageContext.servletContext.contextPath}/professor/all" method="post">
			<label>Ime:</label> <input class="form-control" type="text" name="id" /><br />
			<input class="btn btn-primary" type="submit" value="Nadji" />
		</form>
	</div>
</body>
</html>