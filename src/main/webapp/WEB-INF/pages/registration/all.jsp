<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
	<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<style type="text/css">
	body{
	margin:20px;
	padding:20px;
	}
	.r{
	display:none;
	}
	</style>
	<script type="text/javascript" language="javascript">
	function check(){
		return confirm("Da li zelite da obrisete ispit?");
	}
	function expand(id){
		$('#'+id).slideToggle();
	}
	$(document).ready(function() {
		$('#sel').on('change', function() {
			$.ajax({
				type: "POST",
				url : "${pageContext.servletContext.contextPath}/registration/all",
				data : {
					rows : $(this).val()
				},
				success : function(result) {
					$("#all").html(result);
				}
			});
		});
	});
		
</script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="float:right;">
	<form action="${pageContext.servletContext.contextPath}/registration" method="post">
		<input type="submit" class="btn btn-success" value="Nazad na meni"/>
	</form>
	</div>
<div id="all">
<p style="color:red;"><%=((request.getAttribute("error")==null)?"":request.getAttribute("error")) %></p>
	<label>Broj rezultata na stranici:</label>
	<select id="sel" >
		<option style="background-color: blue;" selected="selected"><%=((request.getAttribute("rows")==null)?"izaberi":request.getAttribute("rows")) %></option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="20">20</option>
	</select>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Datum prijave ispita</th>
				<th scope="col">Ime predmeta</th>
				<th scope="col">Ime studenta</th>
				<th scope="col"></th>
				<th scope="col"></th>
				<th scope="col"></th>
			</tr>
		</thead>
		<c:forEach items="${registrationAll}" var="item">
			<tr>
			
				<td>${item.date}</td>
				<td>${item.exam.subject.name}</td>
				<td>${item.student.firstName} ${item.student.lastName}</td>
				<td><button class="btn btn-success" id="${item.id}" onclick="expand('tr${item.id}');">Detalji</button></td>
				<td><form action="${pageContext.servletContext.contextPath}/registration/edit" method="post">
						<input type="hidden" name="idEdit" value="${item.id}" /><input
							class="btn btn-primary" type="submit" value="Izmeni" />
					</form></td>
				<td><form action="${pageContext.servletContext.contextPath}/registration/delete" method="post" onsubmit="return check()">
						<input type="hidden" name="idRemove" value="${item.id}" /><input
							class="btn btn-danger" type="submit" value="Obrisi" />
					</form></td>
					
			</tr>
			<tr class="r" id="tr${item.id}"><td><b>Detalji:</b><br/>
			<p>Indeks studenta: ${item.student.indexNumber}<br/></p>
			<p>Opis predmeta: ${item.exam.subject.description}<br/></p>
			<p>Godina i semestar: ${item.exam.subject.yearOfStudy}. godina, ${item.exam.subject.semester} semestar<br/></p>
			<p>Profesor: ${item.exam.professor.firstName} ${item.exam.professor.lastName}<br/></p>
			<p>Datum ispita: ${item.exam.date}<br/></p>
			</td></tr>
		</c:forEach>
	</table>
	<p style="color:red;"><%=request.getAttribute("message") %></p>
	</div>
</body>
</html>