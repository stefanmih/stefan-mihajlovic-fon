package stefan.mihajlovic.fon.project1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * This class is responsible for configuring web application, such as
 * {@link ViewResolver}, {@link ViewControllerRegistry} etc.
 * 
 * @author Stefan
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "stefan.mihajlovic.fon.project1.controller"
		// ,"stefan.mihajlovic.fon.project1.repository"
})
public class MyWebContextConfig implements WebMvcConfigurer {
//	@Bean
//	BeanNameUrlHandlerMapping beanNameUrlHandlerMapping() {
//		return new BeanNameUrlHandlerMapping();
//	}
//	@Bean
//	public CityController cityController() {
//		return new CityController();
//	}
//	
//	@Bean
//	public LoginController loginController() {
//		return new LoginController();
//	}
//	@Bean
//	public SimpleUrlHandlerMapping simpleUrlHandlerMapping() {
//		SimpleUrlHandlerMapping simpleUrlHandlerMapping=new SimpleUrlHandlerMapping();
//		Map<String, Object> map=new HashMap<String, Object>();
//		map.put("/city", cityController());
//		map.put("/login", loginController());
//		simpleUrlHandlerMapping.setUrlMap(map);
//		return simpleUrlHandlerMapping;
//	}
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:/login");
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
}
