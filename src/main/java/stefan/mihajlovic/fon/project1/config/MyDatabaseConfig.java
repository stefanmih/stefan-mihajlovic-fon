package stefan.mihajlovic.fon.project1.config;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This class is responsible for configuring relational database to work with. It configures 
 * {@link EntityManager} for persistence context, jdbc driver, hibernate and jpa for 
 * manipulating database data (creating tables, updating...)
 * @author Stefan
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "stefan.mihajlovic.fon.project1.repository" })
@PropertySources({ @PropertySource("classpath:database.properties") })
public class MyDatabaseConfig {
	@Autowired
	private Environment env;

	@Bean
	public DataSource datasource() {
		DriverManagerDataSource datasource = new DriverManagerDataSource();
		datasource.setDriverClassName(env.getProperty("driver"));
		datasource.setUrl(env.getProperty("url"));
		datasource.setUsername(env.getProperty("usr"));
		datasource.setPassword(env.getProperty("pw"));
		return datasource;
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(datasource());
		em.setPackagesToScan(new String[] { "stefan.mihajlovic.fon.project1.entity" });
		JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(jpaVendorAdapter);

		em.setJpaProperties(getAdditionProperties());
		return em;
	}

	private Properties getAdditionProperties() {
		Properties properties = new Properties();
		// properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("javax.persistence.schema-generation.database.action", "create");
		return properties;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
}
