package stefan.mihajlovic.fon.project1.controller;

import java.util.stream.Collectors;

import javax.naming.SizeLimitExceededException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.StudentDto;
import stefan.mihajlovic.fon.project1.entity.StudentEntity;
import stefan.mihajlovic.fon.project1.repository.CityRepository;
import stefan.mihajlovic.fon.project1.repository.StudentRepository;

@Controller
@RequestMapping(value = "/student", method = { RequestMethod.GET, RequestMethod.POST })
public class StudentController {
	@Autowired
	CityRepository cityRepository;

//	@Autowired
//	ProfessorRepository professorRepository;

	@Autowired
	StudentRepository studentRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("student home");
		req.setAttribute("cityAll", cityRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("student/add");
		modelAndView.addObject("StudentDto", new StudentDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public ModelAndView add(@Valid @ModelAttribute(name = "StudentDto") StudentDto studentDto,
			BindingResult bindingResult, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("redirect:/student/all");
		if (bindingResult.hasErrors()) {
			modelAndView.addObject(studentDto);
			modelAndView.setViewName("/student/add");
			request.setAttribute("cityAll", cityRepository.getAll());
			return modelAndView;
		}
		if (studentRepository.findByEmail(studentDto.getEmail()).size() != 0) {
			throw new SizeLimitExceededException(
					"Vec postoji student sa tim email-om. <a href=\"/project1/student/add\">Nazad</a>");
		}
		if (studentRepository.findByIndex(studentDto.getIndexNumber()).size() != 0) {
			throw new SizeLimitExceededException(
					"Vec postoji student sa tim indeksom. <a href=\"/project1/student/add\">Nazad</a>");
		}
		StudentEntity StudentEntity = new StudentEntity();
		StudentEntity.setId(studentDto.getId());
		StudentEntity.setAddress(studentDto.getAddress());
		StudentEntity.setCity(cityRepository.findById(studentDto.getCity().getCode()));
		StudentEntity.setEmail(studentDto.getEmail());
		StudentEntity.setFirstName(studentDto.getFirstName());
		StudentEntity.setLastName(studentDto.getLastName());
		StudentEntity.setPhone(studentDto.getPhone());
		StudentEntity.setIndexNumber(studentDto.getIndexNumber());
		StudentEntity.setCurrentYearOfStudy(studentDto.getCurrentYearOfStudy());
		studentRepository.add(StudentEntity);
		return modelAndView;
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("student/all");
		modelAndView.addObject("StudentDto", new StudentDto());
		int rows = 5;
		if (request.getParameter("rows") != null) {
			rows = Integer.parseInt(request.getParameter("rows"));
		}
		System.out.println(request.getParameter("rows"));
		if (request.getParameter("id") != null) {
			request.setAttribute("studentAll", studentRepository.findByName(request.getParameter("id")).stream()
					.limit(rows).collect(Collectors.toList()));
		} else {
			request.setAttribute("studentAll",
					studentRepository.getAll().stream().limit(rows).collect(Collectors.toList()));
		}
		request.setAttribute("message", "Broj rezultata za prikaz: " + rows);
		return modelAndView;
	}

	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("student/find");
		modelAndView.addObject("studentDto", new StudentDto());
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
		String attr = request.getParameter("idEdit");
		StudentEntity studentEntity = studentRepository.findById(Long.parseLong(attr));
		ModelAndView modelAndView = new ModelAndView("student/add");
		StudentDto studentDto = new StudentDto();
		studentDto.setId(studentEntity.getId());
		studentDto.setAddress(studentEntity.getAddress());
		studentDto.setEmail(studentEntity.getEmail());
		studentDto.setFirstName(studentEntity.getFirstName());
		studentDto.setLastName(studentEntity.getLastName());
		studentDto.setPhone(studentEntity.getPhone());
		studentDto.setIndexNumber(studentEntity.getIndexNumber());
		studentDto.setCurrentYearOfStudy(studentEntity.getCurrentYearOfStudy());
		modelAndView.addObject("StudentDto", studentDto);
		request.setAttribute("oldId", attr);
		request.setAttribute("cityAll", cityRepository.getAll());
		return modelAndView;
	}

	@PostMapping(value = "delete")
	public String remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		req.setAttribute("message",
				"Obrisan Student: " + studentRepository.findById(Long.parseLong(req.getParameter("idRemove"))));
		studentRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return "redirect:/student/all";
	}

	@ExceptionHandler(SizeLimitExceededException.class)
	public ModelAndView error1(HttpServletRequest request, HttpServletResponse response, SizeLimitExceededException e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}

	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public ModelAndView error2(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error",
				"Nije moguce obrisati studenta jer postoji ispit koji je prijavio. <a href=\"/project1/student/all\">Nazad</a>");
		return modelAndView;
	}
}
