package stefan.mihajlovic.fon.project1.controller;

import java.util.stream.Collectors;

import javax.naming.SizeLimitExceededException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.ProfessorDto;
import stefan.mihajlovic.fon.project1.entity.ProfessorEntity;
import stefan.mihajlovic.fon.project1.repository.CityRepository;
import stefan.mihajlovic.fon.project1.repository.ProfessorRepository;
import stefan.mihajlovic.fon.project1.repository.TitleRepository;

@Controller
@RequestMapping(value = "/professor", method = { RequestMethod.GET, RequestMethod.POST })
public class ProfessorController {
	@Autowired
	CityRepository cityRepository;

	@Autowired
	ProfessorRepository professorRepository;

	@Autowired
	TitleRepository titleRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("professor home");
		req.setAttribute("cityAll", cityRepository.getAll());
		req.setAttribute("titleAll", titleRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("professor/add");
		modelAndView.addObject("ProfessorDto", new ProfessorDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public ModelAndView add(@Valid @ModelAttribute(name = "ProfessorDto") ProfessorDto professorDto,
		 BindingResult errors,HttpServletRequest request) throws Exception {
		ModelAndView modelAndView=new ModelAndView("redirect:/professor/all");
		if (errors.hasErrors()) {
			System.out.println(errors.getFieldError());
			modelAndView.addObject(professorDto);
			modelAndView.setViewName("/professor/add");
			request.setAttribute("cityAll", cityRepository.getAll());
			request.setAttribute("titleAll", titleRepository.getAll());
			return modelAndView;
		}
		if(professorRepository.findByEmail(professorDto.getEmail()).size()!=0) {
			throw new SizeLimitExceededException("Vec postoji profesor sa tim email-om. <a href=\"/project1/professor/add\">Nazad</a>");
		}
		System.out.println(professorDto);
		ProfessorEntity professorEntity = new ProfessorEntity();
		professorEntity.setId(professorDto.getId());
		professorEntity.setAddress(professorDto.getAddress());
		professorEntity.setCity(cityRepository.findById(professorDto.getCity().getCode()));
		professorEntity.setEmail(professorDto.getEmail());
		professorEntity.setFirstName(professorDto.getFirstName());
		professorEntity.setLastName(professorDto.getlastName());
		professorEntity.setPhone(professorDto.getPhone());
		professorEntity.setReelectionDate(professorDto.getReelectionDate());
		professorEntity.setTitle(titleRepository.findById(professorDto.getTitle().getId()));
		professorRepository.add(professorEntity);
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
		String attr = request.getParameter("idEdit");

		ModelAndView modelAndView = new ModelAndView("professor/add");
		ProfessorDto professorDto=new ProfessorDto();
		ProfessorEntity professorEntity=professorRepository.findById(Long.parseLong(attr));
		professorDto.setAddress(professorEntity.getAddress());
		professorDto.setEmail(professorEntity.getEmail());
		professorDto.setFirstName(professorEntity.getFirstName());
		professorDto.setId(professorEntity.getId());
		professorDto.setlastName(professorEntity.getLastName());
		professorDto.setPhone(professorEntity.getPhone());
		professorDto.setReelectionDate(professorEntity.getReelectionDate());
		modelAndView.addObject("ProfessorDto", professorDto);
		request.setAttribute("oldId", attr);
		request.setAttribute("cityAll", cityRepository.getAll());
		request.setAttribute("titleAll", titleRepository.getAll());
		return modelAndView;
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		modelAndView.addObject("ProfessorDto", new ProfessorDto());
		request.setAttribute("message", "");
		int rows = 5;
		if (request.getParameter("rows") != null) {
			rows = Integer.parseInt(request.getParameter("rows"));
		}
		System.out.println(request.getParameter("rows"));
		if (request.getParameter("id") != null) {
			System.out.println(professorRepository.findByName(request.getParameter("id")));
			request.setAttribute("professorAll", professorRepository.findByName(request.getParameter("id")).stream()
					.limit(rows).collect(Collectors.toList()));
		} else {
			request.setAttribute("professorAll",
					professorRepository.getAll().stream().limit(rows).collect(Collectors.toList()));
		}
		request.setAttribute("message", "Broj rezultata za prikaz: " + rows);
		return modelAndView;
	}

	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("professor/find");
		modelAndView.addObject("professorDto", new ProfessorDto());
		return modelAndView;
	}

	@PostMapping(value = "delete")
	public String remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		req.setAttribute("message",
				"Obrisan profesor: " + professorRepository.findById(Long.parseLong(req.getParameter("idRemove"))));
		professorRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return "redirect:/professor/all";
	}

	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public ModelAndView error2(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error", "Nije moguce obrisati profesora jer postoji ispit koji drzi profesor. <a href=\"/project1/professor/all\">Nazad</a>");
		return modelAndView;
	}
	@ExceptionHandler(SizeLimitExceededException.class)
	public ModelAndView error1(HttpServletRequest request, HttpServletResponse response, SizeLimitExceededException e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error",  e.getMessage());
		return modelAndView;
	}

}
