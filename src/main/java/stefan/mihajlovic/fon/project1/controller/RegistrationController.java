
package stefan.mihajlovic.fon.project1.controller;

import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.RegistrationDto;
import stefan.mihajlovic.fon.project1.entity.RegistrationEntity;
import stefan.mihajlovic.fon.project1.repository.ExamRepository;
import stefan.mihajlovic.fon.project1.repository.RegistrationRepository;
import stefan.mihajlovic.fon.project1.repository.StudentRepository;
import stefan.mihajlovic.fon.project1.repository.SubjectRepository;

@Controller
@RequestMapping(value = "/registration", method = { RequestMethod.GET, RequestMethod.POST })
public class RegistrationController {
	@Autowired
	ExamRepository examRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	RegistrationRepository registrationRepository;

	@Autowired
	SubjectRepository subjectRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		req.setAttribute("examAll", examRepository.getAll());
		req.setAttribute("studentAll", studentRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("registration/add");
		modelAndView.addObject("RegistrationDto", new RegistrationDto());
		return modelAndView;
	}

	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("registration/find");
		modelAndView.addObject("RegistrationDto", new RegistrationDto());
		return modelAndView;
	}

	public int dateDifference(Date date1, Date date2) {
		long startTime = date1.getTime();
		long endTime = date2.getTime();
		long diffTime = endTime - startTime;
		long diffDays = diffTime / (1000 * 60 * 60 * 24);
		return (int) diffDays;
	}

	@PostMapping(value = "add")
	public String add(@ModelAttribute(name = "RegistrationDto") RegistrationDto registrationDto,
			@RequestParam(name = "save") String action, SessionStatus sessionStatus, HttpServletRequest request)
			throws ValidationException {
		boolean addObject = true;
		if (!(dateDifference(new Date(System.currentTimeMillis()),
				examRepository.findById(registrationDto.getExam().getId()).getDate()) > 0
				&& dateDifference(new Date(System.currentTimeMillis()),
						examRepository.findById(registrationDto.getExam().getId()).getDate()) < 7)
				&& registrationDto.getExam().getId() == registrationDto.getExam().getId()) {
			addObject = false;
		}
		if (studentRepository.findById(registrationDto.getStudent().getId()).getCurrentYearOfStudy() < subjectRepository
				.findById(examRepository.findById(registrationDto.getExam().getId()).getSubject().getId()).getYearOfStudy()) {
			throw new ValidationException(
					"Student ne moze prijaviti ispit koji nije slusao. <a href=\"/project1/registration\">Nazad</a>");
		}
			for (RegistrationEntity r : registrationRepository.getAll()) {
			
				if (registrationDto.getExam().getId() == r.getExam().getId()
						&& (dateDifference(new Date(System.currentTimeMillis()), r.getExam().getDate()) > 0
								&& dateDifference(new Date(System.currentTimeMillis()), r.getExam().getDate()) < 7)
						&& registrationDto.getStudent().getId() == r.getStudent().getId()
						&& registrationDto.getStudent().getId() == r.getStudent().getId()) {
					throw new ValidationException(
							"Student je vec prijavio ispit. <a href=\"/project1/registration\">Nazad</a>");
				}
			}
		if (addObject || !request.getParameter("id").equals("")) {
			RegistrationEntity registrationEntity = new RegistrationEntity();
			registrationEntity.setDate(new Date(System.currentTimeMillis()));
			registrationEntity.setExam(examRepository.findById(registrationDto.getExam().getId()));
			registrationEntity.setId(registrationDto.getId());
			registrationEntity.setStudent(studentRepository.findById(registrationDto.getStudent().getId()));
			registrationRepository.add(registrationEntity);
		} else {
			throw new ValidationException(
					"Nije moguce dodati prijavu ispita jer je datum prijave istekao ili nije pocela prijava. <a href=\"/project1/registration\">Nazad</a>");
		}
		return "redirect:/registration/all";
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("registration/all");
		modelAndView.addObject("RegistrationDto", new RegistrationDto());
		int rows = 5;
		if (request.getParameter("rows") != null) {
			rows = Integer.parseInt(request.getParameter("rows"));
		}
		System.out.println(request.getParameter("rows"));
		if (request.getParameter("id") != null) {
			System.out.println(registrationRepository.findByName(request.getParameter("id")));
			request.setAttribute("registrationAll", registrationRepository.findByName(request.getParameter("id"))
					.stream().limit(rows).collect(Collectors.toList()));
		} else {
			request.setAttribute("registrationAll",
					registrationRepository.getAll().stream().limit(rows).collect(Collectors.toList()));
		}
		request.setAttribute("message", "Broj rezultata za prikaz: " + rows);
		return modelAndView;
	}

	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
		String attr = request.getParameter("idEdit");
		ModelAndView modelAndView = new ModelAndView("registration/add");
		RegistrationDto registrationDto=new RegistrationDto();
		RegistrationEntity registrationEntity=registrationRepository.findById(Long.parseLong(attr));
		registrationDto.setId(registrationEntity.getId());
		modelAndView.addObject("RegistrationDto", registrationDto);
		request.setAttribute("oldId", attr);
		request.setAttribute("examAll", examRepository.getAll());
		request.setAttribute("studentAll", studentRepository.getAll());
		return modelAndView;
	}

	@PostMapping(value = "delete")
	public String remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		RegistrationEntity registrationEntity = registrationRepository
				.findById(Long.parseLong(req.getParameter("idRemove")));
		req.setAttribute("message", "Obrisan Ispit: " + registrationEntity);
		registrationRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return "redirect:/registration/all";
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView error(HttpServletRequest request, HttpServletResponse response, ValidationException e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}

}
