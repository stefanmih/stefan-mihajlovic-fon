package stefan.mihajlovic.fon.project1.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {
	@RequestMapping(value = "/",method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest req,HttpServletResponse resp) {
		ModelAndView modelAndView=new ModelAndView("authentication/login");
		return modelAndView;
	}
	@RequestMapping(value = "/",method = RequestMethod.GET)
	public ModelAndView signIn(HttpServletRequest req,HttpServletResponse resp) {
		ModelAndView modelAndView=new ModelAndView("authentication/login");
		return modelAndView;
	}
}
