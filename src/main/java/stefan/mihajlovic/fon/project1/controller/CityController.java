
package stefan.mihajlovic.fon.project1.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.domain.City;
import stefan.mihajlovic.fon.project1.entity.CityEntity;
import stefan.mihajlovic.fon.project1.repository.CityRepository;

/**
 * This class is web controller that maps to /city. It uses both {@link RequestMethod} get and post.
 * 
 * @author Stefan
 *
 */
@Controller
@RequestMapping(value = "/city", method = { RequestMethod.GET, RequestMethod.POST })
public class CityController {
	@Autowired
	CityRepository cityRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("city/add");
		modelAndView.addObject("City", new City());
		return modelAndView;
	}
	
	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("city/find");
		modelAndView.addObject("City", new City());
		return modelAndView;
	}
	
	@PostMapping(value = "remove")
	public ModelAndView remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("city/all");
		modelAndView.addObject("City", new City());
		if(req.getParameter("id")!=null) {
		cityRepository.remove(Long.parseLong(req.getParameter("id")));
		req.setAttribute("cityAll", cityRepository.getAll());
		}
		return modelAndView;
	}

	@PostMapping(value = "add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request.getParameter("code"));
		System.out.println(request.getParameter("name"));
		ModelAndView modelAndView = new ModelAndView("city/all");
		modelAndView.addObject("City", new City());
		CityEntity city = new CityEntity(Long.parseLong(request.getParameter("code")), request.getParameter("name"));
		request.setAttribute("city", city);
		// CityRepository cityRepository=new CityRepository();
		System.out.println(city);
		cityRepository.add(city);
		System.out.println(cityRepository.getAll());
		modelAndView.addObject("City", new City());
		request.setAttribute("cityAll", cityRepository.getAll());
		return modelAndView;
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("city/all");
		modelAndView.addObject("City", new City());
		if (request.getParameter("id") != null) {
			request.setAttribute("cityAll", Arrays.asList(cityRepository.findById(Long.parseLong(request.getParameter("id")))));
		} else {
			request.setAttribute("cityAll", cityRepository.getAll());
		}
		return modelAndView;
	}
	@ExceptionHandler(Exception.class)
	public ModelAndView error(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView=new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}
}
