
package stefan.mihajlovic.fon.project1.controller;

import java.util.Date;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.ExamDto;
import stefan.mihajlovic.fon.project1.entity.ExamEntity;
import stefan.mihajlovic.fon.project1.repository.ExamRepository;
import stefan.mihajlovic.fon.project1.repository.ProfessorRepository;
import stefan.mihajlovic.fon.project1.repository.SubjectRepository;

/**
 * This class is web controller that maps to /exam. It uses both
 * {@link RequestMethod} get and post. It has autowired repositories for
 * manipulating data in relational database (CRUD operations). Also it works as
 * converter from data transfer objects to entity object.
 * 
 * @author Stefan
 *
 */
@Controller
@RequestMapping(value = "/exam", method = { RequestMethod.GET, RequestMethod.POST })
public class ExamController {
	@Autowired
	ExamRepository examRepository;

	@Autowired
	ProfessorRepository professorRepository;

	@Autowired
	SubjectRepository subjectRepository;

	/**
	 * This method maps to default mapping for controller. It sends attributes to
	 * jsp page provided with {@link ModelAndView}
	 * 
	 * @param req  Server request
	 * @param resp Server response
	 * @return {@link ModelAndView} from certain page that contains specific data
	 *         transfer object.
	 */
	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		req.setAttribute("professorAll", professorRepository.getAll());
		req.setAttribute("subjectAll", subjectRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("exam/add");
		modelAndView.addObject("ExamDto", new ExamDto());
		return modelAndView;
	}

	/**
	 * Method calculates difference in days from two given {@link Date} dates. Other
	 * values like hours, minutes and seconds doesn't matter.
	 * 
	 * @param date1 First date
	 * @param date2 Second date
	 * @return Returns 0 if dates are equal in days, less than 0 if first
	 *         {@link Date} is greater than second {@link Date} or greater than 0 if
	 *         second {@link Date} is greater than first {@link Date}
	 */
	public int dateDifference(Date date1, Date date2) {
		long startTime = date1.getTime();
		long endTime = date2.getTime();
		long diffTime = endTime - startTime;
		long diffDays = diffTime / (1000 * 60 * 60 * 24);
		return (int) diffDays;
	}

	/**
	 * This method maps to /exam/add and it is used for converting from data
	 * transfer object to entity object that needs to be stored in database through
	 * {@link ExamRepository}. It has attributes validation before processing with
	 * storing.
	 * 
	 * @param examDto       {@link ExamDto} object that is sent from specific jsp
	 *                      page
	 * @param action        Check when submit is activated
	 * @param sessionStatus Session status
	 * @param request       Server request
	 * @return {@link String} which represents redirect page after storage is
	 *         completed without errors.
	 * @throws ValidationException It represents custom message if some requirements
	 *                             are not met.
	 */
	@PostMapping(value = "add")
	public String add(@ModelAttribute(name = "ExamDto") ExamDto examDto, @RequestParam(name = "save") String action,
			SessionStatus sessionStatus, HttpServletRequest request) throws ValidationException {
		boolean addObject = true;
		for (ExamEntity e : examRepository.getAll()) {
			if (dateDifference(e.getDate(), new Date(System.currentTimeMillis())) <= 0
					&& (e.getSubject().getId() == examDto.getSubject().getId())) {
				System.out.println(dateDifference(e.getDate(), examDto.getDate()));
				addObject = false;
			}
		}
		if (addObject || !request.getParameter("id").equals("")) {
			ExamEntity examEntity = new ExamEntity();
			examEntity.setDate(examDto.getDate());
			examEntity.setId(examDto.getId());
			examEntity.setProfessor(professorRepository.findById(examDto.getProfessor().getId()));
			examEntity.setSubject(subjectRepository.findById(examDto.getSubject().getId()));
			examRepository.add(examEntity);

		} else {
			throw new ValidationException("Nije moguce dodati ispit ponovo jer nije prosao datum prethodnog.");
		}
		return "redirect:/exam/all";
	}

	/**
	 * This method maps to /exam/all and it is used for obtaining all or requested
	 * data from specific repository. It also contains server side pagination.
	 * 
	 * @param request  Server request
	 * @param response Server response
	 * @return {@link ModelAndView} to itself with custom attributes given to
	 *         {@link HttpServletRequest} parameter
	 */
	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("exam/all");
		modelAndView.addObject("ExamDto", new ExamDto());
		int rows = 5;
		if (request.getParameter("rows") != null) {
			rows = Integer.parseInt(request.getParameter("rows"));
		}
		System.out.println(request.getParameter("rows"));
		if (request.getParameter("id") != null) {
			request.setAttribute("examAll", examRepository.findByName(request.getParameter("id")).stream().limit(rows)
					.collect(Collectors.toList()));
		} else {
			request.setAttribute("examAll", examRepository.getAll().stream().limit(rows).collect(Collectors.toList()));
		}
		request.setAttribute("message", "Broj rezultata za prikaz: " + rows);
		return modelAndView;
	}

	/**
	 * This method maps to /exam/edit and it is used for editing specific data from
	 * database. It finds entity in database by primary key and edit it if possible.
	 * 
	 * @param request  Server request
	 * @param response Server response
	 * @return {@link ModelAndView} to page for adding data except that it sends
	 *         primary key so {@link EntityManager} can merge data.
	 */
	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
		String attr = request.getParameter("idEdit");
		ModelAndView modelAndView = new ModelAndView("exam/add");
		ExamDto examDto=new ExamDto();
		ExamEntity examEntity=examRepository.findById(Long.parseLong(attr));
		examDto.setDate(examEntity.getDate());
		examDto.setId(examEntity.getId());
		modelAndView.addObject("ExamDto", examDto);
		request.setAttribute("oldId", attr);
		request.setAttribute("professorAll", professorRepository.getAll());
		request.setAttribute("subjectAll", subjectRepository.getAll());
		return modelAndView;
	}

	/**
	 * This method maps to /exam/delete and it removes data from database, if possible. 
	 * If data cannot be removed it throws exception.
	 * @param req Server request
	 * @param resp Server response
	 * @return {@link String} that represents redirect page for obtaining all data.
	 */
	@PostMapping(value = "delete")
	public String remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ExamEntity examEntity = examRepository.findById(Long.parseLong(req.getParameter("idRemove")));
		req.setAttribute("message", "Obrisan Ispit: " + examEntity);
		examRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return "redirect:/exam/all";
	}
	
	/**
	 * This method respond to {@link ValidationException} if exists. It redirects to error page
	 *  and shows message from exception.
	 * @param e Exception type
	 * @param request Server request
	 * @param response Server response
	 * @return {@link ModelAndView} to error page with error message as {@link HttpServletRequest} attribute.
	 */
	@ExceptionHandler(ValidationException.class)
	public ModelAndView error(ValidationException e, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}

	/**
	 * This method respond to {@link org.hibernate.exception.ConstraintViolationException} if exists. It redirects to error page
	 *  and shows custom message provided in method.
	 * @param e Exception type
	 * @param request Server request
	 * @param response Server response
	 * @return {@link ModelAndView} to error page with error message as {@link HttpServletRequest} attribute.
	 */
	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public ModelAndView error2(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error",
				"Nije moguce obrisati ispit jer postoji prijava. <a href=\"/project1/exam/all\">Nazad</a>");
		return modelAndView;
	}

}
