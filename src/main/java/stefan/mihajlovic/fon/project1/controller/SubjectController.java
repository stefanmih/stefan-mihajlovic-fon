
package stefan.mihajlovic.fon.project1.controller;


import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.SubjectDto;
import stefan.mihajlovic.fon.project1.entity.SubjectEntity;
import stefan.mihajlovic.fon.project1.repository.SubjectRepository;

@Controller
@RequestMapping(value = "/subject", method = { RequestMethod.GET, RequestMethod.POST })
public class SubjectController {
	@Autowired
	SubjectRepository subjectRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("subject/add");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		return modelAndView;
	}
	
	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("subject/find");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public ModelAndView add(
			@Valid @ModelAttribute(name="SubjectDto")SubjectDto subjectDto, 
			BindingResult bindingResult, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView=new ModelAndView("redirect:/subject/all");
		if(bindingResult.hasErrors()) {
			modelAndView.addObject(subjectDto);
			modelAndView.setViewName("/subject/add");
			return modelAndView;
		}
		SubjectEntity subjectEntity = new SubjectEntity();
		subjectEntity.setDescription(subjectDto.getDescription());
		subjectEntity.setId(subjectDto.getId());
		subjectEntity.setName(subjectDto.getName());
		subjectEntity.setSemester(subjectDto.getSemester());
		subjectEntity.setYearOfStudy(subjectDto.getYearOfStudy());
		subjectRepository.add(subjectEntity);
		return modelAndView;
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("subject/all");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		int rows=5;
		if(request.getParameter("rows")!=null) {
			rows=Integer.parseInt(request.getParameter("rows"));
		}
		System.out.println(request.getParameter("rows"));
		if (request.getParameter("id") != null) {
			request.setAttribute("subjectAll", subjectRepository.findByName(request.getParameter("id")).stream().limit(rows).collect(Collectors.toList()));
		} else {
			request.setAttribute("subjectAll", subjectRepository.getAll().stream().limit(rows).collect(Collectors.toList()));
		}
		request.setAttribute("message", "Broj rezultata za prikaz: "+rows);
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request,HttpServletResponse response){
		String attr=request.getParameter("idEdit");
		ModelAndView modelAndView=new ModelAndView("subject/add");
		SubjectEntity subjectEntity = subjectRepository.findById(Long.parseLong(attr));
		SubjectDto subjectDto=new SubjectDto();
		subjectDto.setDescription(subjectEntity.getDescription());
		subjectDto.setId(subjectEntity.getId());
		subjectDto.setName(subjectEntity.getName());
		subjectDto.setSemester(subjectEntity.getSemester());
		subjectDto.setYearOfStudy(subjectEntity.getYearOfStudy());
		modelAndView.addObject("SubjectDto",subjectDto);
		request.setAttribute("oldId", attr);
		request.setAttribute("cityAll", subjectRepository.getAll());
		return modelAndView;
	}
	
	@PostMapping(value = "delete")
	public String remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		req.setAttribute("message", "Obrisan Predmet: "+subjectRepository.findById(Long.parseLong(req.getParameter("idRemove"))));
		subjectRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return "redirect:/subject/all";
	}
	
	
	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public ModelAndView error2(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView = new ModelAndView("error/error");
		request.setAttribute("error", "Nije moguce obrisati predmet jer postoji ispit iz tog predmeta. <a href=\"/project1/subject/all\">Nazad</a>");
		return modelAndView;
	}
}
