package stefan.mihajlovic.fon.project1.domain;

import java.util.Date;

public class Exam {
	Long id;
	private Subject subject;
	private Professor professor;
	private Date date;
	public Exam(Long id,Subject subject, Professor professor, Date date) {
		super();
		this.id=id;
		this.subject = subject;
		this.professor = professor;
		this.date = date;
	}
	public Exam() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Exam [subject=" + subject + ", professor=" + professor + ", date=" + date + "]";
	}
	
	
}
