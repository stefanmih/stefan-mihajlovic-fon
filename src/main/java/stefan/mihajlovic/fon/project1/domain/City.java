package stefan.mihajlovic.fon.project1.domain;

public class City {
	Long code;
	String name;

	public City(Long code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public City() {
		super();
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "City [code=" + code + ", name=" + name + "]";
	}

}
