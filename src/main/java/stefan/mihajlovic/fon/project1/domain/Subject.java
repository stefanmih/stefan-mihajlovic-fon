package stefan.mihajlovic.fon.project1.domain;

public class Subject {
	private Long id;
	private String name;
	private String description;
	private Long yearOfStudy;
	private String semester;
	public Subject(Long id, String name, String description, Long yearOfStudy, String semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}
	public Subject() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getYearOfStudy() {
		return yearOfStudy;
	}
	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy=" + yearOfStudy
				+ ", semester=" + semester + "]";
	}
	
	
}
