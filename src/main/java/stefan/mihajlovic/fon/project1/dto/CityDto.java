package stefan.mihajlovic.fon.project1.dto;

import javax.validation.constraints.NotNull;

public class CityDto {
	@NotNull(message = "Morete uneti ptt broj")
	Long code;
	String name;
	public CityDto(Long code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
	public CityDto() {
		super();
	}
	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "CityDto [code=" + code + ", name=" + name + "]";
	}
	
	
	
}
