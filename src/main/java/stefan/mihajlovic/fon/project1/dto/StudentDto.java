package stefan.mihajlovic.fon.project1.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentDto {
	Long id;
	@NotEmpty(message = "Ne sme biti prazno")
	@Size(min = 10, max = 20, message = "Nije ispravno uneto")
	private String indexNumber;
	@NotEmpty(message = "Ne sme biti prazno")
	@Size(min = 3, max = 30, message = "Nije ispravna duzina")
	private String firstName;
	@NotEmpty(message = "Ne sme biti prazno")
	@Size(min = 3, max = 30, message = "Nije ispravna duzina")
	private String lastName;
	private String email;
	@Size(min = 3, max = 50, message = "Nije ispravna duzina")
	private String address;
	private CityDto city;
	@Size(min = 6, max = 15, message = "Nije ispravna duzina")
	private String phone;
	@NotNull(message = "Ne sme biti prazno")
	private Long currentYearOfStudy;

	public StudentDto(Long id, String indexNumber, String firstName, String lastName, String email, String address,
			CityDto city, String phone, Long currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public StudentDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	@Override
	public String toString() {
		return "StudentDto [indexNumber=" + indexNumber + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", address=" + address + ", city=" + city + ", phone=" + phone
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}

}
