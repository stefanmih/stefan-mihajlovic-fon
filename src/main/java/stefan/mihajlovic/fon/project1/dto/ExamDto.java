package stefan.mihajlovic.fon.project1.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ExamDto {
	Long id;
	private SubjectDto subject;
	private ProfessorDto professor;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	public ExamDto(Long id,SubjectDto subject, ProfessorDto professor, Date date) {
		super();
		this.id=id;
		this.subject = subject;
		this.professor = professor;
		this.date = date;
	}
	public ExamDto() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	public ProfessorDto getProfessor() {
		return professor;
	}
	public void setProfessor(ProfessorDto professor) {
		this.professor = professor;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ExamDto [subject=" + subject + ", professor=" + professor + ", date=" + date + "]";
	}
	
	
}
