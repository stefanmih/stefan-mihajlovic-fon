package stefan.mihajlovic.fon.project1.dto;

public class RegistrationDto {
	private Long id;
	private StudentDto student;
	private ExamDto exam;
	public RegistrationDto(Long id, StudentDto student, ExamDto exam) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
	}
	public RegistrationDto() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public StudentDto getStudent() {
		return student;
	}
	public void setStudent(StudentDto student) {
		this.student = student;
	}
	public ExamDto getExam() {
		return exam;
	}
	public void setExam(ExamDto exam) {
		this.exam = exam;
	}
	@Override
	public String toString() {
		return "RegistrationDto [id=" + id + ", student=" + student + ", exam=" + exam + "]";
	}
	
}
