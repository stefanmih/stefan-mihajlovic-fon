package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.StudentEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link StudentEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "studentJpaRepository")
@Transactional
@Service
public class StudentRepository {
	@PersistenceContext
	EntityManager entityManager;
	/**
	 * Adding {@link StudentEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link StudentEntity} object that needs to be stored.
	 */
	public void add(StudentEntity c) {
		entityManager.merge(c);
	}
	
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link StudentEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<StudentEntity> getAll() {
		return entityManager.createNamedQuery("StudentEntity.findAll").getResultList();
	}
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link StudentEntity} objects that are in
	 *         table and have given key as {@link StudentEntity} specific attribute. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<StudentEntity> findByName(String id) {
		return entityManager.createQuery("select s from StudentEntity s where s.firstName like :p").setParameter("p", id).getResultList();
	} 
	
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link StudentEntity} objects that are in
	 *         table and have given key as {@link StudentEntity} specific attribute. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<StudentEntity> findByEmail(String id) {
		return entityManager.createQuery("select s from StudentEntity s where s.email like :p").setParameter("p", id).getResultList();
	} 
	/**
	 * Obtaining unique {@link StudentEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link StudentEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public StudentEntity findById(Long id) {
		return entityManager.find(StudentEntity.class, id);
	}
	
	/**
	 * Removes {@link StudentEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	public void remove(Long id) {
		entityManager.remove(findById(id));
		
	}
	
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param index key for finding (not primary key)
	 * @return Returns {@link List} of all {@link StudentEntity} objects that are in
	 *         table and have given key as {@link StudentEntity} specific attribute. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<StudentEntity> findByIndex(String index) {
		return entityManager.createQuery("select s from StudentEntity s where s.indexNumber like :p").setParameter("p", index).getResultList();
	}
}
