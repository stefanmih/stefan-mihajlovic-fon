package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.SubjectEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link SubjectEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "subjectJpaRepository")
@Transactional
@Service
public class SubjectRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	/**
	 * Adding {@link SubjectEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link SubjectEntity} object that needs to be stored.
	 */
	public void add(SubjectEntity c) {
		entityManager.merge(c);
	}
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link SubjectEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<SubjectEntity> getAll() {
		return entityManager.createNamedQuery("SubjectEntity.findAll").getResultList();
	}
	/**
	 * Obtaining unique {@link SubjectEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link SubjectEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public SubjectEntity findById(Long id) {
		return entityManager.find(SubjectEntity.class, id);
	}
	
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link SubjectEntity} objects that are in
	 *         table and have given key as {@link SubjectEntity} specific attribute. If there is no 
	 *         object found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<SubjectEntity> findByName(String id) {
		return entityManager.createQuery("select s from SubjectEntity s where s.name like :p").setParameter("p", id).getResultList();
	} 
	
	/**
	 * Removes {@link SubjectEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
