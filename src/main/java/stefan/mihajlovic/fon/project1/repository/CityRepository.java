package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.CityEntity;


/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link CityEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "cityJpaRepository")
@Transactional
@Service
public class CityRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	/**
	 * Adding {@link CityEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link CityEntity} object that needs to be stored.
	 */
	public void add(CityEntity c) {
		entityManager.persist(c);
	}
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link CityEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<CityEntity> getAll() {
		return entityManager.createNamedQuery("CityEntity.findAll").getResultList();
	}
	
	/**
	 * Obtaining unique {@link CityEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link CityEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public CityEntity findById(Long id) {
		return entityManager.find(CityEntity.class, id);
	}
	
	/**
	 * Removes {@link CityEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
