package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.TitleEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link TitleEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "titleJpaRepository")
@Transactional
@Service
public class TitleRepository {
	@PersistenceContext
	EntityManager entityManager;
	/**
	 * Adding {@link TitleEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link TitleEntity} object that needs to be stored.
	 */
	public void add(TitleEntity c) {
		entityManager.merge(c);
	}
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link TitleEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<TitleEntity> getAll() {
		return entityManager.createNamedQuery("TitleEntity.findAll").getResultList();
	}
	/**
	 * Obtaining unique {@link TitleEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link TitleEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public TitleEntity findById(Long id) {
		return entityManager.find(TitleEntity.class, id);
	} 
}