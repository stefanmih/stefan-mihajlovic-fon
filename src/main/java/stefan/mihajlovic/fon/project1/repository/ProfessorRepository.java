package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.ProfessorEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link ProfessorEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "professorJpaRepository")
@Transactional
@Service
public class ProfessorRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	/**
	 * Adding {@link ProfessorEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link ProfessorEntity} object that needs to be stored.
	 */
	public void add(ProfessorEntity c) {
		entityManager.merge(c);
	}
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link ProfessorEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<ProfessorEntity> getAll() {
		return entityManager.createNamedQuery("ProfessorEntity.findAll").getResultList();
	}
	/**
	 * Obtaining unique {@link ProfessorEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link ProfessorEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public ProfessorEntity findById(Long id) {
		return entityManager.find(ProfessorEntity.class, id);
	}
	
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link ProfessorEntity} objects that are in
	 *         table and have given key as {@link ProfessorEntity} specific attribute. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<ProfessorEntity> findByEmail(String id) {
		return entityManager.createQuery("select p from ProfessorEntity p where p.email like :p").setParameter("p", id).getResultList();
	} 
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link ProfessorEntity} objects that are in
	 *         table and have given key as {@link ProfessorEntity} specific attribute. If there is no 
	 *         object found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<ProfessorEntity> findByName(String id) {
		return entityManager.createQuery("select p from ProfessorEntity p where p.firstName like :p").setParameter("p", id).getResultList();
	} 
	/**
	 * Removes {@link ProfessorEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
