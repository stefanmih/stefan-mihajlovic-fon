package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.ExamEntity;
import stefan.mihajlovic.fon.project1.entity.SubjectEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link ExamEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "examJpaRepository")
@Transactional
@Service
public class ExamRepository {
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Adding {@link ExamEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link ExamEntity} object that needs to be stored.
	 */
	public void add(ExamEntity c) {
		entityManager.merge(c);
	}

	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link ExamEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<ExamEntity> getAll() {
		return entityManager.createNamedQuery("ExamEntity.findAll").getResultList();
	}

	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link ExamEntity} objects that are in
	 *         table and have given key as {@link SubjectEntity} specific attribute. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<ExamEntity> findByName(String id) {
		return entityManager.createQuery("select p from ExamEntity p where p.subject.name like :p")
				.setParameter("p", id).getResultList();
	}

	/**
	 * Obtaining unique {@link ExamEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link ExamEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public ExamEntity findById(Long id) {
		return entityManager.find(ExamEntity.class, id);
	}

	/**
	 * Removes {@link ExamEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
