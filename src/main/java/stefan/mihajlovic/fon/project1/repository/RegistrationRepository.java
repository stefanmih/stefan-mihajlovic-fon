package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.RegistrationEntity;

/**
 * This class provides methods for data manipulation under any relational database configured with 
 * {@link EntityManager}. It has basic CRUD operations (create, read, update, delete) over {@link RegistrationEntity}
 *  class.
 * @author Stefan 
 *
 */
@Repository(value = "registrationJpaRepository")
@Transactional
@Service
public class RegistrationRepository {
	@PersistenceContext
	EntityManager entityManager;
	/**
	 * Adding {@link RegistrationEntity} to database. It uses entity manager from
	 * persistence context for storing objects
	 * 
	 * @param c {@link RegistrationEntity} object that needs to be stored.
	 */
	public void add(RegistrationEntity c) {
		entityManager.merge(c);
	}
	/**
	 * Obtaining all data from database.
	 * 
	 * @return Returns {@link List} of all {@link RegistrationEntity} objects that are in
	 *         table. If there are no 
	 *         objects found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<RegistrationEntity> getAll() {
		return entityManager.createNamedQuery("RegistrationEntity.findAll").getResultList();
	}
	
	/**
	 * Obtaining specific data from database.
	 * 
	 * @param id key for finding (not primary key)
	 * @return Returns {@link List} of all {@link RegistrationEntity} objects that are in
	 *         table and have given key as {@link RegistrationEntity} specific attribute. If there is no 
	 *         object found, {@link List} is empty.
	 */
	@SuppressWarnings("unchecked")
	public List<RegistrationEntity> findByName(String id) {
		return entityManager.createQuery("select p from RegistrationEntity p where p.exam.subject.name like :p").setParameter("p", id).getResultList();
	} 
	/**
	 * Obtaining unique {@link RegistrationEntity} object from database if exists.
	 * 
	 * @param id primary key
	 * @return returns {@link RegistrationEntity} object if found, else returns
	 *         <code>null</code>
	 */
	public RegistrationEntity findById(Long id) {
		return entityManager.find(RegistrationEntity.class, id);
	}

	/**
	 * Removes {@link RegistrationEntity} object from database if possible, else throws 
	 * {@link IllegalArgumentException} or {@link TransactionRequiredException}.
	 * 
	 * @param id primary key
	 */
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
