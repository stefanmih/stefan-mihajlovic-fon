package stefan.mihajlovic.fon.project1.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn; 
import javax.persistence.NamedQuery;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="exam")
@NamedQuery(name="ExamEntity.findAll", query="SELECT e FROM ExamEntity e")
public class ExamEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST },fetch = FetchType.EAGER)
	@JoinColumn(name="exam_subject")
	private SubjectEntity subject;
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST },fetch = FetchType.EAGER)
	@JoinColumn(name="professor_subject")
	private ProfessorEntity professor;
	private Date date;
	public ExamEntity(Long id,SubjectEntity subject, ProfessorEntity professor, Date date) {
		super();
		this.id=id;
		this.subject = subject;
		this.professor = professor;
		this.date = date;
	}
	public ExamEntity() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public SubjectEntity getSubject() {
		return subject;
	}
	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}
	public ProfessorEntity getProfessor() {
		return professor;
	}
	public void setProfessor(ProfessorEntity professor) {
		this.professor = professor;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ExamEntity [subject=" + subject + ", professor=" + professor + ", date=" + date + "]";
	}
	
	
}
