package stefan.mihajlovic.fon.project1.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "registration")
@NamedQuery(name="RegistrationEntity.findAll", query="SELECT r FROM RegistrationEntity r")
public class RegistrationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "registration_student")
	private StudentEntity student;
	@ManyToOne
	@JoinColumn(name = "registration_exam")
	private ExamEntity exam;
	private Date date;
	public RegistrationEntity(Long id, StudentEntity student, ExamEntity exam, Date date) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
		this.date = date;
	}
	public RegistrationEntity() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public StudentEntity getStudent() {
		return student;
	}
	public void setStudent(StudentEntity student) {
		this.student = student;
	}
	public ExamEntity getExam() {
		return exam;
	}
	public void setExam(ExamEntity exam) {
		this.exam = exam;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "RegistrationEntity [id=" + id + ", student=" + student + ", exam=" + exam + ", date=" + date + "]";
	}
	
}
