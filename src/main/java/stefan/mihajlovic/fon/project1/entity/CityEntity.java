package stefan.mihajlovic.fon.project1.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "city")
@NamedQuery(name = "CityEntity.findAll", query = "SELECT c FROM CityEntity c")
public class CityEntity {
	@Id
	Long code;
	String name;

	public CityEntity(Long code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public CityEntity() {
		super();
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityEntity [code=" + code + ", name=" + name + "]";
	}

}
